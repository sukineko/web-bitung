<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keamanan extends Model
{
    protected $table = 'keamanan';
    protected $fillable = ['title','subtitle','details'];
    // public $timestamps = false;
    use HasFactory;
}
