<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lingkungan_hidup extends Model
{
    protected $table = 'lingkungan_hidup';
    protected $fillable = ['title','subtitle','details'];
    // public $timestamps = false;

    use HasFactory;
}
