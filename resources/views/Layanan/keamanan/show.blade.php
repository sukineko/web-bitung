@extends('layouts.index') 
@section('content')

<main id="main">

<!-- ======= Breadcrumbs ======= -->
<div class="breadcrumbs" data-aos="fade-in">
  <div class="container">
    <h2>keamanan</h2>
    <p>{{ $keamanans->title }}</p>
  </div>
</div><!-- End Breadcrumbs -->

<!-- ======= Cource Details Section ======= -->
<section id="course-details" class="course-details">
  <div class="container" data-aos="fade-up">

    <div class="row">
   
        <div class="col-lg-8">
        <!-- <img src="/assets/img/course-details.jpg" class="img-fluid" alt=""> -->
        <h3> {{ $keamanans->subtitle }} </h3>
        @php
          echo $keamanans->details
        @endphp	
      </div>
    </div>

  </div>
</section><!-- End Cource Details Section -->

</main><!-- End #main -->



@endsection