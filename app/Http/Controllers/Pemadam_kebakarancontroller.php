<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Pemadam_kebakaran;
  
class Pemadam_kebakaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('password.confirm')->only(['create']);
        // $this->middleware('password.confirm')->only(['show']);
        $this->middleware('password.confirm')->only(['edit']);

    }

    public function index()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Pemadam_kebakarans = Pemadam_kebakaran::get();
        // dd($Pemadam_kebakarans);
        /// mengirimkan variabel $Pemadam_kebakarans ke halaman views Pemadam_kebakarans/index.blade.php
        /// include dengan number index
        return view('layananDashboard.Pemadam_kebakarans.index', compact('Pemadam_kebakarans'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
  
    public function create()
    {
        /// menampilkan halaman create
        return view('layananDashboard.Pemadam_kebakarans.create');
    }
  
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'details' => 'required',
        ]);
         
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        Pemadam_kebakaran::create($request->all());
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('pemadam-kebakaran.index')
                        ->with('success','Pemadam_kebakaran created successfully.');
    }
  
    public function show(Pemadam_kebakaran $pemadam_kebakaran)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Pemadam_kebakarans.show',$post->id) }}
        return view('layananDashboard.Pemadam_kebakarans.show',compact('pemadam_kebakaran'));
    }
  
    public function edit(Pemadam_kebakaran $pemadam_kebakaran)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Pemadam_kebakarans.edit',$post->id) }}
        return view('layananDashboard.Pemadam_kebakarans.edit',compact('pemadam_kebakaran'));
    }
  
    public function update(Request $request, Pemadam_kebakaran $Pemadam_kebakaran)
    {
        /// membuat validasi untuk title dan content wajib diisi
        // $request->validate([
        //     'title' => 'required',
        //     'content' => 'required',
        // ]);
         
        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        $Pemadam_kebakaran->update($request->all());
         
        /// setelah berhasil mengubah data
        return redirect()->route('pemadam-kebakaran.index')->with('success','Pemadam_kebakaran updated successfully');
    }
  
    public function destroy(Pemadam_kebakaran $pemadam_kebakaran)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        $pemadam_kebakaran->delete();
  
        return redirect()->route('pemadam-kebakaran.index')
                        ->with('success','Pemadam_kebakaran deleted successfully');
    }
}
