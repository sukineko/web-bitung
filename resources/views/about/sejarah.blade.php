@extends('layouts.index') 
@section('content')

  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
      <div class="container">
        <h2>Tentang</h2>
        <!-- <p>Sejarah Kota Bitung</p> -->
      </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="row">
         
            <h3>Sejarah Kota Bitung</h3>
            <p class="fst-italic">
            Nama BITUNG diambil dari nama pohon bitung (Latin: Hivia Hospital), tumbuhan tropis  yang banyak tumbuh di pesisir pantai Nusantara hingga ke Madagaskar. Pohon bitung dalam bahasa Belanda adalah Stevige Koroestige Boom (Sangirees-Nederlands Woordenboek, Steller, 1959). Dalam bahasa Sangihe disebut tariang. Nama-nama lokal lainnya adalah bogem, butong, butun, pertun, putat laut, bitung, talise, hutun.<br>
            Pohon bitung sempat disebut sebagai pohon perdamaian. Predikat pohon perdamaian ini diberikan oleh Presiden Soeharto pada saat perayaan Hari Lingkungan Hidup tahun 1986. 
            Nama bitung diberikan kepada kota Bitung sejak pantai yang ditumbuhi pohon bitung menjadi tempat berteduh dan persinggahan nelayan daerah sekitar di akhir abad ke-18 atau awal abad ke-19. Karena ramainya nelayan yang datang ke area itu, maka lama-kelamaan tempat ini menjadi sebuah pemukiman.<br>
            Konon area persinggahan  dan menjadi pemukiman nelayan itu terletak di kawasan yang saat ini telah menjadi area terminal BBM Pertamina Kota Bitung hingga ke pelabuhan samudra. Di sinilah berawalnya perkembangan perkampungan nelayan kemudian menjadi Desa Bitung. Ketika menjadi desa, penduduknya berbaur etnis  Minahasa (sub etnis Tonsea khususnya) dan Sangihe-Talaud. Kemudian datang lagi dari etnis Maluku Utara, Mongondow dan beberapa etnis lainnya.<br>
            Sebagai sebuah desa, Bitung berada di bawah onderdstrik (kecamatan) Kauditan merupakan bagian dari distrik Tonsea. Desa Bitung pertama kali dipimpin oleh hukum tua (kepala desa) Arkelaus Sompotan dan memimpin selama kurang lebih 25 tahun. Di bawah kepemimpinan Arkelaus Sompotan, penduduk Bitung terus bertambah dan penganut agama pun beragam. Mayoritasnya beragama Kristen kemudian menyusul penganut Islam. Tokoh-tokoh pendiri desa Bitung yang beragama Kristen antara lain Simon Tudus, Elias Lontoh Sompotan, Daniel Mais Pongoh, Hendrikus Langie Langelo, Martinus Langelo, Andries Rompis, Mais Pantow, Benyamin Wangi, Andries Hendrik Dulag Kansil, Theopilus Bawotong, Frederik Tindatu dan Yesaya Malalutan.<br>
            Pada 1 Januari 1918 Desa Bitung diakui oleh Pemerintah Hindia Belanda sebagai sebuah negeri. Namun  beslit pengesahannya baru dikeluarkan pada 1 Januari 1928. Diperkirakan, penerbitan beslit ini terjadi setelah tahun 1926 Theopilus Bawotong, Frederik Tindatu dan Hendrik Dulag Kansil mewakili warga Desa Bitung menghadap Hukum Besar Tonsea di Airmadidi.<br>
            Pada tahun 1927 Elias Lontoh Sompotan, cucu mantu Simon Tudus diangkat menjadi Hukum Tua sampai tahun 1928, dan diganti oleh H.L. Langelo.
            Setelah proklamasi kemerdekaan Indinesia, pada 1 Juli 1947 Bitung menjadi sebuah distrik bawahan (onderdistrik) yang terpisah dari distrik bawahan Tonsea dengan luas wilayah 19.870 Ha, terdiri dari 13.428 jiwa tersebar pada 11 desa. Tahun 1964 dengan SK Gubernur Kepala Daerah Tingkat I Sulawesi Utara Nomor 244 Tahun 1964, Bitung ditetapkan menjadi satu Kecamatan dengan jumlah penduduk 32.000 jiwa tersebar pada 28 desa dengan luas wilayah 29,79 km².<br>
            Oleh pemerintah orde lama, pada tahun 1967 dibentuklah Kantor Penghubung atau Wakil Bupati Minahasa di Bitung, sebagai koordinator seluruh Pemerintahan dan Pembangunan. Lalu diikuti pembentukan Badan Koordinasi Pelaksanaan Pembangunan Bitung oleh Gubernur Propinsi Sulawesi Utara pada tahun 1968. Pada bulan April 1971 Bupati Minahasa menetapkan Struktur Organisasi dan Tata Kerja dari Penghubung Bupati Minahasa di Bitung.
            Melihat pertumbuhan dan perkembangan wilayah kecamatan Bitung yang semakin pesat, maka pada tanggal 2 Juli 1974, Gubernur Propinsi Sulawesi Utara mengangkat Wempi A. Worang untuk menjabat tiga jabatan sekaligus, yakni Penghubung Bupati, Camat Bitung dan Kepala Dinas Pembangunan Bitung.<br>
            Melihat kesiapan kecamatan Bitung yang cukup matang, berdasarkan Peraturan Pemerintah Nomor 4 Tahun 1975, maka pada 10 April 1975 Kecamatan Bitung diresmikan sebagai Kota Administratif pertama di Indonesia, dengan wali kota administratif adalah Wempi A. Worang. Kala itu luas wilayah Bitung mencapai 304 km² terdiri dari 3 kecamatan dengan 35 desa. Ketiga kecamatan itu meliputi Bitung Utara (Danowudu hingga Batuputih), Kecamatan Bitung Tengah (Tanjung Merah hinga Kasawari) dan Kecamatan Bitung Selatan (seluruh desa di Pulau Lembeh).<br>
            Kerja keras dan perjuangan wali kota administratif ketiga Drs. S.H. Sarundajang, memunculkan julukan Bitung  sebagai Kota Lima Dimensi (Kota Pelabuhan, Kota Industri, Kota Perdagangan, Kota Pariwisata dan Kota Pemerintahan) yang kemudian berubah menjadi Kota Serbadimensi. Usaha dari wali kota Sarundajang menghasilkan keputusan pemerintah Indonesia dengan menetapkan kota administratif Bitung  menjadi Kota Madya dan diresmikan pada tanggal 10 Oktober 1990. Peresmiannya dilakuan oleh Menteri Dalam Negeri, Rudini, berdasarkan Undang-Undang Nomor 7 Tahun 1990, dengan luas wilayah 304 km², 3 kecamatan dan 44 kelurahan.<br>
            Pada masa kepemimpinan wali kota Sarundajang, Kota Bitung mengalami pemekaran dari 3 kecamatan menjadi 4 kecamatan. Hal ini didasarkan pada PP Nomor 43 Tahun 1995 tanggal 6 Desember 1995. Kecamatan yang baru mekar adalah Kecamatan Bitung Timur sebagai hasil pemekaran dari Kecamatan Bitung Tengah. Dengan demikian Kota Madya Bitung menjadi 4 wilayah kecamatan.<br>
            Selanjutnya pada masa kepemimpinan wali kota Milton Kansil, Kecamatan Bitung Tengah mengalami  pemekaran. Pada 14 Desember 2001 berdasarkan Keputusan Walikota Bitung Nomor 100 Tahun 2001, bertambahlah kecamatan baru yakni Kecamatan Bitung Barat.<br>
            Terakhir, pemekaran di kota Bitung terjadi pada 10 Oktober 2007. Pemekaran terjadi untuk wilayah kecamatan dan kelurahan. Dari semula 5 kecamatan menjadi 8 kecamatan dan dari 44 kelurahan menjadi 69 kelurahan. Kedelapan kecamatan itu meliputi Kecamatan Ranowulu dengan 11 kelurahan, Kecamatan Matuari dengan 8 kelurahan, Kecamatan Girian dengan 7 kelurahan, Kecamatan Madidir dengan 8 kelurahan, Kecamatan Maesa dengan 8 kelurahan, Kecamatan Aertembaga dengan 10 kelurahan, Kecamatan Lembeh Utara meliputi 10 kelurahan dan Kecamatan Lembeh Selatan meliputi 7 kelurahan.<br>
            Secara geografis, wilayah kota Bitung sebagian besar daratannya merupakan daerah berbukit dan gunung, terletak pada posisi di antara 1o23’23” – 1o35’39” LU dan 125o1’43” – 125o18’13” BT. Di sebelah Selatan berbatasan dengan Laut Maluku, di sebelah Utara berbatasan dengan Kecamatan Likupang dan Kecamatan Dimembe (Kabupaten Minahasa Utara), Sebelah Timur berbatasan dengan Laut Maluku sedangkan sebelah Barat berbatasan dengan Kecamatan Kauditan (Kabupaten Minahasa Utara).<br>

            
        </div>

      </div>
    </section><!-- End About Section -->

   


  </main><!-- End #main -->


	@endsection
