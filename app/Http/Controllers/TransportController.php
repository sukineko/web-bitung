<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Transport;
  
class TransportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('password.confirm')->only(['create']);
        // $this->middleware('password.confirm')->only(['show']);
        $this->middleware('password.confirm')->only(['edit']);

    }

    public function index()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Transports = Transport::get();
        // dd($Transports);
        /// mengirimkan variabel $Transports ke halaman views Transports/index.blade.php
        /// include dengan number index
        return view('layananDashboard.Transports.index', compact('Transports'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
  
    public function create()
    {
        /// menampilkan halaman create
        return view('layananDashboard.Transports.create');
    }
  
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'details' => 'required',
        ]);
         
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        Transport::create($request->all());
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('transport.index')
                        ->with('success','Transport created successfully.');
    }
  
    public function show(Transport $transport)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Transports.show',$post->id) }}
        return view('layananDashboard.Transports.show',compact('transport'));
    }
  
    public function edit(Transport $transport)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Transports.edit',$post->id) }}
        return view('layananDashboard.Transports.edit',compact('transport'));
    }
  
    public function update(Request $request, Transport $Transport)
    {
        /// membuat validasi untuk title dan content wajib diisi
        // $request->validate([
        //     'title' => 'required',
        //     'content' => 'required',
        // ]);
         
        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        $Transport->update($request->all());
         
        /// setelah berhasil mengubah data
        return redirect()->route('transport.index')->with('success','Transport updated successfully');
    }
  
    public function destroy(Transport $transport)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        $transport->delete();
  
        return redirect()->route('transport.index')
                        ->with('success','Transport deleted successfully');
    }
}
