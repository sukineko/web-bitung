<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Kesehatan;
  
class KesehatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('password.confirm')->only(['create']);
        // $this->middleware('password.confirm')->only(['show']);
        $this->middleware('password.confirm')->only(['edit']);

    }

    public function index()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Kesehatans = Kesehatan::get();
        // dd($Kesehatans);
        /// mengirimkan variabel $Kesehatans ke halaman views Kesehatans/index.blade.php
        /// include dengan number index
        return view('layananDashboard.Kesehatans.index', compact('Kesehatans'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
  
    public function create()
    {
        /// menampilkan halaman create
        return view('layananDashboard.Kesehatans.create');
    }
  
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'details' => 'required',
        ]);
         
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        Kesehatan::create($request->all());
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('kesehatan.index')
                        ->with('success','Kesehatan created successfully.');
    }
  
    public function show(Kesehatan $kesehatan)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Kesehatans.show',$post->id) }}
        return view('layananDashboard.Kesehatans.show',compact('kesehatan'));
    }
  
    public function edit(Kesehatan $kesehatan)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Kesehatans.edit',$post->id) }}
        return view('layananDashboard.Kesehatans.edit',compact('kesehatan'));
    }
  
    public function update(Request $request, Kesehatan $Kesehatan)
    {
        /// membuat validasi untuk title dan content wajib diisi
        // $request->validate([
        //     'title' => 'required',
        //     'content' => 'required',
        // ]);
         
        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        $Kesehatan->update($request->all());
         
        /// setelah berhasil mengubah data
        return redirect()->route('kesehatan.index')->with('success','Kesehatan updated successfully');
    }
  
    public function destroy(Kesehatan $kesehatan)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        $kesehatan->delete();
  
        return redirect()->route('kesehatan.index')
                        ->with('success','Kesehatan deleted successfully');
    }
}
