@extends('layouts.index') 
@section('content')

  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
      <div class="container">
        <h2>Tentang</h2>
        <!-- <p>Sejarah Kota Bitung</p> -->
      </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <!-- <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
            <img src="assets/img/about.jpg" class="img-fluid" alt="">
          </div> -->
          <!-- <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content"> -->
            <h3>ARTI LAMBANG KOTA BITUNG</h3>
            <p class="fst-italic">
            1. Bentuk, Warna dan Bagian – bagian lambang

a. Lambang Daerah berbentuk segi lima, bis luar berwarna merah dengan warna biru laut.

b. Di tengah lambang terdapat gambar setangkai daun pohon Bitung berjumlah 17 helai berwarna hijau, yang dihubungkan oleh lingkaran kecil berjumlah 8 berbis hitam dengan setangkai bunga kelapa yang nampak belum mekar berjumlah 45 berwarna kuning emas. Di tengahnya terdapat bentuk lukisan yang terdiri dari :

· Dua ekor ikan berwarna perak;

· Sebuah jangkar kapal berwarna perak;

· Sebuah bangunan industri (ditengah-edt);

· Sebuah bangunan kantor Pemerintahan (sebelah kiri pakai tirisan-edt);

· Sebuah bangunan perdagangan (sebelah kanan tanpa tirisan-edt);

· Seekor burung manguni berwarna hitam;

· Gunung Dua Sudara berwarna hijau.

c. Di bagian bawah terdapat pita putih ber bis merah bertuliskan Kota Bitung.

 

2. Gambar – gambar pada Lambang Daerah

Arti gambar-gambar pada lambang daerah

a. Segi lima mengandung arti Pancasila sebagai dasar negara kesatuan Republik Indonesia dan sebagai falsafah hidup dan pedoman hidup bangsa Indonesia yang menjiwai dan mendasari segala segi kehidupan berbangsa dan bernegara.

b. Setangkai daun pohon Bitung berarti sejarah nama kota Bitung diambil dari nama pohon Bitung dengan jumlah helai daunnya 17, melambangkan tanggal proklamasi kemerdekaan Republik Indonesia.

c. Lingkaran kecil berjumlah 8 buah melambangkan bulan proklamasi kemerdekaan Republik Indonesia.

d. Setangkai mayang kelapa dengan 45 bunga belum mekar melambangkan tahun kemerdekaan Republik Indonesia, juga sebagai pertanda kemakmuran, kemurnian dan keluhuran masyarakat dalam mencapai cita-cita masyarakat adil dan makmur.

e. Dua ekor ikan berwarna perak, melambangkan kekayaan hasil laut wilayah kota Bitung sebagai kota penghasil dan pengekspor ikan.

f. Sebuah jangkar kapal mengandung arti kota Bitung sebagai kota pelabuhan yang merupakan pintu gerbang utama melalui laut.

g. Sebuah bangunan industri sebagai salah satu dimensi kota Bitung dengan ditetapkannya sebagai pusat kawasan industri Sulawesi Utara.

h. Sebuah bangunan kantor pemerintahan mengandung arti semua potensi yang ada merupakan tanggung jawab pemerintah dalam menjalankan secara efektif dan efisien bagi kepentingan pembangunan daerah.

i. Sebuah bangunan perdagangan yang merupakan dimensi lain dari kota Bitung yang sangat menentukan dinamika kehidupan perekonomian.

j. Gambar gunung dua sudara dengan warna hijau melambangkan sebagai keadaan geografis dimana kota Bitung terletak pada kaki gunung duasudara yang subur dan makmur.

k. Burung manguni mengandung arti keperkasaan, kewaspadaan dan mewarisi nilai-nilai budaya dengan semangat mapalus.

 

3. Penjelasan Warna

a. Warna biru laut berarti ketentraman, kebahagiaan, kesetiaan, kehormatan, keluhuran dan sebagai tanda kota pelabuhan yang mempunyai laut yang luas.

b. Warna kuning berarti kemakmuran, kejayaan dan kemurnian dalam melaksanakan tanggung jawab dan kewajibannya terhadap bangsa dan negara.

c. Warna hijau berarti kesuburan alam sebagai potensi kehidupan masyarakatnya dalam mendukung pembangunan kota Bitung.

d. Warna perak berarti kejayaan dan kemegahan daerah.

e. Warna merah berarti keberanian serta berjiwa perwira untuk membela bangsa dan negara.

f. Warna putih berarti kesucian hati, memiliki citra yang bersih dalam menjalankan tugas.

 

g. Warna hitam berarti memiliki sifat persatuan dan kesatuan dalam bernegara.

            <!-- </p>
            <ul>
              <li><i class="bi bi-check-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i class="bi bi-check-circle"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li><i class="bi bi-check-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
            </ul>
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
            </p> -->

          <!-- </div> -->
        </div>

      </div>
    </section><!-- End About Section -->

   


  </main><!-- End #main -->


	@endsection
