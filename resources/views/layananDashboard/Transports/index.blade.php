@extends('layoutsDashboard.index') 
@section('content')
  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tables Transport</h1>
<p class="mb-4"></p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6> -->

        <div class="float-right"  style="margin-buttom:100px"  >
            <a class="btn btn-success" href="{{ route('transport.create') }}"> Create Post</a>
        </div>
    </div>
    
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               
            
                <thead>
                    <tr>
                        <th>title</th>
                        <th style="text-align:center; width:20%" >action</th>

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>title</th>
                        <th style="text-align:center; width:20%" >action</th>
                    </tr>
                </tfoot>
                <tbody>
                @foreach ($Transports as $transport)
                    <tr>
                        <td> {{$transport->title}} </td>
                        <td class="text-center">
                            <form action="{{ route('transport.destroy',$transport->id) }}" method="POST">
                                <a class="btn btn-info btn-sm" href="{{ route('transport.show',$transport->id) }} ">Show  </a>
                                <a class="btn btn-primary btn-sm" href="{{ route('transport.edit',$transport ->id) }}">Edit</a>         
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                            </form>
                        </td>
                        
                    </tr>
                @endforeach
                    
                    
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
<!-- /.container-fluid -->

@endsection
