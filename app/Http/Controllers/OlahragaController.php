<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Olahraga;
  
class OlahragaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('password.confirm')->only(['create']);
        // $this->middleware('password.confirm')->only(['show']);
        $this->middleware('password.confirm')->only(['edit']);

    }

    public function index()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Olahragas = Olahraga::get();
        // dd($Olahragas);
        /// mengirimkan variabel $Olahragas ke halaman views Olahragas/index.blade.php
        /// include dengan number index
        return view('layananDashboard.Olahragas.index', compact('Olahragas'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
  
    public function create()
    {
        /// menampilkan halaman create
        return view('layananDashboard.Olahragas.create');
    }
  
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'details' => 'required',
        ]);
         
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        Olahraga::create($request->all());
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('olahraga.index')
                        ->with('success','Olahraga created successfully.');
    }
  
    public function show(Olahraga $olahraga)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Olahragas.show',$post->id) }}
        return view('layananDashboard.Olahragas.show',compact('olahraga'));
    }
  
    public function edit(Olahraga $olahraga)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Olahragas.edit',$post->id) }}
        return view('layananDashboard.Olahragas.edit',compact('olahraga'));
    }
  
    public function update(Request $request, Olahraga $Olahraga)
    {
        /// membuat validasi untuk title dan content wajib diisi
        // $request->validate([
        //     'title' => 'required',
        //     'content' => 'required',
        // ]);
         
        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        $Olahraga->update($request->all());
         
        /// setelah berhasil mengubah data
        return redirect()->route('olahraga.index')->with('success','Olahraga updated successfully');
    }
  
    public function destroy(Olahraga $olahraga)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        $olahraga->delete();
  
        return redirect()->route('olahraga.index')
                        ->with('success','Olahraga deleted successfully');
    }
}
