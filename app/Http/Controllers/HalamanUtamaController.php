<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kesehatan;
use App\Models\Keamanan;
use App\Models\Olahraga;
use App\Models\Transport;
use App\Models\Pemadam_kebakaran;
use App\Models\Lingkungan_hidup;

class HalamanUtamaController extends Controller
{
    public function index()
    {

        return view("home.index");
    }

    public function dashboard()
    {
        
        $kesehatans = Kesehatan::all()->count();
        $keamanans = Keamanan::all()->count();
        $olahraga = Olahraga::all()->count();
        $transport = Transport::all()->count();
        $pemadam_kebakaran = Pemadam_kebakaran::all()->count();
        $lingkungan_hidup = Lingkungan_hidup::all()->count();

        return view("layananDashboard.index",compact('kesehatans','keamanans','olahraga','transport','pemadam_kebakaran','lingkungan_hidup'));
    }

    public function sejarah()
    {
        return view("about.sejarah");
    }
        
    public function arti_lambang()
    {
        return view("about.arti_lambang");
    }

    public function contact()
    {
        return view("contact.index");
    }

    public function index_kesehatan()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Kesehatans = Kesehatan::get();
        // dd($Kesehatans);
        /// mengirimkan variabel $Kesehatans ke halaman views Kesehatans/index.blade.php
        /// include dengan number index
        return view('layanan.Kesehatan.index', compact('Kesehatans'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function show_kesehatan($title)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Kesehatans.show',$post->id) }}
        $kesehatans = Kesehatan::where('title', $title)->first();
        return view('layanan.Kesehatan.show',compact('kesehatans'));
    }
    public function index_keamanan()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Keamanans = Keamanan::get();
        // dd($Kesehatans);
        /// mengirimkan variabel $Kesehatans ke halaman views Kesehatans/index.blade.php
        /// include dengan number index
        return view('layanan.Keamanan.index', compact('Keamanans'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function show_keamanan($title)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Kesehatans.show',$post->id) }}
        $keamanans = Keamanan::where('title', $title)->first();
        return view('layanan.Keamanan.show',compact('keamanans'));
    }
    public function index_olahraga()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Olahragas = Olahraga::get();
        // dd($Olahragas);
        /// mengirimkan variabel $Olahragas ke halaman views Olahragas/index.blade.php
        /// include dengan number index
        return view('layanan.Olahraga.index', compact('Olahragas'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function show_olahraga($title)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Olahragas.show',$post->id) }}
        $olahragas = Olahraga::where('title', $title)->first();
        return view('layanan.Olahraga.show',compact('olahragas'));
    }
    public function index_transport()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Transports = Transport::get();
        // dd($Transports);
        /// mengirimkan variabel $Transports ke halaman views Transports/index.blade.php
        /// include dengan number index
        return view('layanan.Transport.index', compact('Transports'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function show_transport($title)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Transports.show',$post->id) }}
        $transports = Transport::where('title', $title)->first();
        return view('layanan.Transport.show',compact('transports'));
    }
    public function index_pemadam_kebakaran()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Pemadam_kebakarans = Pemadam_kebakaran::get();
        // dd($Pemadam_kebakarans);
        /// mengirimkan variabel $Pemadam_kebakarans ke halaman views Pemadam_kebakarans/index.blade.php
        /// include dengan number index
        return view('layanan.pemadam_kebakaran.index', compact('Pemadam_kebakarans'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function show_pemadam_kebakaran($title)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Pemadam_kebakarans.show',$post->id) }}
        $pemadam_kebakarans = Pemadam_kebakaran::where('title', $title)->first();
        return view('layanan.pemadam_kebakaran.show',compact('pemadam_kebakarans'));
    }
    
    public function index_lingkungan_hidup()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Lingkungan_hidups = Lingkungan_hidup::get();
        // dd($Lingkungan_hidups);
        /// mengirimkan variabel $Lingkungan_hidups ke halaman views Lingkungan_hidups/index.blade.php
        /// include dengan number index
        return view('layanan.lingkungan_hidup.index', compact('Lingkungan_hidups'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function show_lingkungan_hidup($title)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Lingkungan_hidups.show',$post->id) }}
        $lingkungan_hidups = Lingkungan_hidup::where('title', $title)->first();
        return view('layanan.lingkungan_hidup.show',compact('lingkungan_hidups'));
    }
}
