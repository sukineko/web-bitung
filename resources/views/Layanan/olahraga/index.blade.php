@extends('layouts.index') 
@section('content')

  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
      <div class="container">
        <h2>Olahraga</h2>
        <p></p>
      </div>
    </div><!-- End Breadcrumbs -->

       <!-- ======= Features Section ======= -->
       <section id="features" class="features" style="margin-top:100px">
      <div class="container" data-aos="fade-up">

        <div class="row" data-aos="zoom-in" data-aos-delay="100">
          @foreach ($Olahragas as $olahraga)
            <div class="col-lg-3 col-md-4">
              <div class="icon-box">
                <i class="" style="color: #ffbb2c;"></i>
                <h3><a href="/layanan/olahraga/{{ $olahraga->title}} "> {{ $olahraga->title }} </a></h3>
              </div> 
            </div>
          @endforeach
        </div>

      </div>
    </section><!-- End Features Section -->
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

      </div>
    </section><!-- End About Section -->

   


  </main><!-- End #main -->


	@endsection
