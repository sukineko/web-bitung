<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=> '1',
            'name' => 'Admin',
            // 'level_id'=> '2',
            'email' => 'admin',
            'password' => bcrypt('sirius'),
            'created_at' => Carbon::now(),
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
