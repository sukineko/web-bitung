<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\HalamanUtamaController;
use App\Http\Controllers\KesehatanController;
use App\Http\Controllers\KeamananController;
use App\Http\Controllers\TransportController;
use App\Http\Controllers\OlahragaController;
use App\Http\Controllers\Pemadam_kebakaranController;
use App\Http\Controllers\Lingkungan_hidupController;


use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::resource('posts', PostController::class);

Route::get('/', function () {
    return view('welcome');
});


Route::get('dashboard', [HalamanUtamaController::class, 'dashboard'])->middleware('auth');

Route::get('logout', [AuthController::class, 'logout']);

Route::get('home', [HalamanUtamaController::class, 'index']);
Route::get('contact', [HalamanUtamaController::class, 'contact']);

// halaman tentang
Route::get('sejarah', [HalamanUtamaController::class, 'sejarah']);
Route::get('arti-lambang', [HalamanUtamaController::class, 'arti_lambang']);
// halaman layanan
Route::get('layanan/kesehatan ', [HalamanUtamaController::class, 'index_kesehatan']);
Route::get('layanan/kesehatan/{title} ', [HalamanUtamaController::class, 'show_kesehatan']);
Route::get('layanan/keamanan ', [HalamanUtamaController::class, 'index_keamanan']);
Route::get('layanan/keamanan/{title} ', [HalamanUtamaController::class, 'show_keamanan']);
Route::get('layanan/olahraga ', [HalamanUtamaController::class, 'index_olahraga']);
Route::get('layanan/olahraga/{title} ', [HalamanUtamaController::class, 'show_olahraga']);
Route::get('layanan/transport ', [HalamanUtamaController::class, 'index_transport']);
Route::get('layanan/transport/{title} ', [HalamanUtamaController::class, 'show_transport']);
Route::get('layanan/pemadam-kebakaran ', [HalamanUtamaController::class, 'index_pemadam_kebakaran']);
Route::get('layanan/pemadam-kebakaran/{title} ', [HalamanUtamaController::class, 'show_pemadam_kebakaran']);
Route::get('layanan/lingkungan-hidup ', [HalamanUtamaController::class, 'index_lingkungan_hidup']);
Route::get('layanan/lingkungan-hidup/{title} ', [HalamanUtamaController::class, 'show_lingkungan_hidup']);

   
//halaman dashboard
// Route::get('/kesehatan/create', [HalamanUtamaController::class, 'create'])->middleware('password.confirm');

Route::resource('keamanan', KeamananController::class)->middleware('auth');
Route::resource('kesehatan', KesehatanController::class)->middleware('auth');
Route::resource('transport', TransportController::class)->middleware('auth');
Route::resource('pemadam-kebakaran',Pemadam_kebakaranController::class)->middleware('auth');
Route::resource('lingkungan-hidup', Lingkungan_hidupController::class)->middleware('auth');
Route::resource('olahraga', OlahragaController::class)->middleware('auth');


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
