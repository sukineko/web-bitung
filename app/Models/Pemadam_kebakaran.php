<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemadam_kebakaran extends Model
{
    protected $table = 'pemadam_kebakaran';
    protected $fillable = ['title','subtitle','details'];
    // public $timestamps = false;

    use HasFactory;
}
