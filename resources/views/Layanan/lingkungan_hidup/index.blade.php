@extends('layouts.index') 
@section('content')

  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
      <div class="container">
        <h2>lingkungan hidup</h2>
        <p></p>
      </div>
    </div><!-- End Breadcrumbs -->

       <!-- ======= Features Section ======= -->
       <section id="features" class="features" style="margin-top:100px">
      <div class="container" data-aos="fade-up">

        <div class="row" data-aos="zoom-in" data-aos-delay="100">
          @foreach ($Lingkungan_hidups as $lingkungan_hidup)
            <div class="col-lg-3 col-md-4">
              <div class="icon-box">
                <i class="" style="color: #ffbb2c;"></i>
                <h3><a href="/layanan/lingkungan-hidup/{{ $lingkungan_hidup->title}} "> {{ $lingkungan_hidup->title }} </a></h3>
              </div> 
            </div>
          @endforeach
        </div>

      </div>
    </section><!-- End Features Section -->
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <!-- <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
            <img src="assets/img/about.jpg" class="img-fluid" alt="">
          </div> -->
          <!-- <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content"> -->
            
            <!-- </p>
            <ul>
              <li><i class="bi bi-check-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i class="bi bi-check-circle"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li><i class="bi bi-check-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
            </ul>
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
            </p> -->

          <!-- </div> -->
        </div>

      </div>
    </section><!-- End About Section -->

   


  </main><!-- End #main -->


	@endsection
