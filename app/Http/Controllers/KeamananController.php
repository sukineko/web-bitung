<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Keamanan;
  
class KeamananController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('password.confirm')->only(['create']);
        // $this->middleware('password.confirm')->only(['show']);
        $this->middleware('password.confirm')->only(['edit']);

    }

    public function index()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Keamanans = Keamanan::get();
        // dd($Keamanans);
        /// mengirimkan variabel $Keamanans ke halaman views Keamanans/index.blade.php
        /// include dengan number index
        return view('layananDashboard.Keamanans.index', compact('Keamanans'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
  
    public function create()
    {
        /// menampilkan halaman create
        return view('layananDashboard.Keamanans.create');
    }
  
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'details' => 'required',
        ]);
         
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        Keamanan::create($request->all());
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('keamanan.index')
                        ->with('success','Keamanan created successfully.');
    }
  
    public function show(Keamanan $keamanan)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Keamanans.show',$post->id) }}
        return view('layananDashboard.Keamanans.show',compact('keamanan'));
    }
  
    public function edit(Keamanan $keamanan)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Keamanans.edit',$post->id) }}
        return view('layananDashboard.Keamanans.edit',compact('keamanan'));
    }
  
    public function update(Request $request, Keamanan $keamanan)
    {
        /// membuat validasi untuk title dan content wajib diisi
        // $request->validate([
        //     'title' => 'required',
        //     'content' => 'required',
        // ]);
         
        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        $keamanan->update($request->all());
         
        /// setelah berhasil mengubah data
        return redirect()->route('keamanan.index')
                        ->with('success','Keamanan updated successfully');
    }
  
    public function destroy(Keamanan $keamanan)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        $keamanan->delete();
  
        return redirect()->route('keamanan.index')
                        ->with('success','Keamanan deleted successfully');
    }
}
