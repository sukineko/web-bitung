<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Lingkungan_hidup;
  
class Lingkungan_hidupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('password.confirm')->only(['create']);
        // $this->middleware('password.confirm')->only(['show']);
        $this->middleware('password.confirm')->only(['edit']);

    }

    public function index()
    {
        /// mengambil data terakhir dan pagination 5 list
        $Lingkungan_hidups = Lingkungan_hidup::get();
        // dd($Lingkungan_hidups);
        /// mengirimkan variabel $Lingkungan_hidups ke halaman views Lingkungan_hidups/index.blade.php
        /// include dengan number index
        return view('layananDashboard.Lingkungan_hidups.index', compact('Lingkungan_hidups'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
  
    public function create()
    {
        /// menampilkan halaman create
        return view('layananDashboard.Lingkungan_hidups.create');
    }
  
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'details' => 'required',
        ]);
         
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        Lingkungan_hidup::create($request->all());
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('pemadam-kebakaran.index')
                        ->with('success','Lingkungan_hidup created successfully.');
    }
  
    public function show(Lingkungan_hidup $lingkungan_hidup)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Lingkungan_hidups.show',$post->id) }}
        return view('layananDashboard.Lingkungan_hidups.show',compact('lingkungan_hidup'));
    }
  
    public function edit(Lingkungan_hidup $lingkungan_hidup)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Lingkungan_hidups.edit',$post->id) }}
        return view('layananDashboard.Lingkungan_hidups.edit',compact('lingkungan_hidup'));
    }
  
    public function update(Request $request, Lingkungan_hidup $Lingkungan_hidup)
    {
        /// membuat validasi untuk title dan content wajib diisi
        // $request->validate([
        //     'title' => 'required',
        //     'content' => 'required',
        // ]);
         
        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        $Lingkungan_hidup->update($request->all());
         
        /// setelah berhasil mengubah data
        return redirect()->route('pemadam-kebakaran.index')->with('success','Lingkungan_hidup updated successfully');
    }
  
    public function destroy(Lingkungan_hidup $lingkungan_hidup)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        $lingkungan_hidup->delete();
  
        return redirect()->route('pemadam-kebakaran.index')
                        ->with('success','Lingkungan_hidup deleted successfully');
    }
}
