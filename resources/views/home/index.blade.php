@extends('layouts.index') 
@section('content')

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex justify-content-center align-items-center">
    <div class="container position-relative" data-aos="zoom-in" data-aos-delay="100">
      <h1>Selamat Datang di,<br> Kota Bitung</h1>
      <!-- <h2>We are team of talented designers making websites with Bootstrap</h2> -->
      <!-- <a href="courses.html" class="btn-get-started">Get Started</a> -->
    </div>
  </section><!-- End Hero -->

  <main id="main">

   <!-- ======= Breadcrumbs ======= -->
   <div class="breadcrumbs">
      <div class="container">
        <h2>Home</h2>
      </div>
    </div><!-- End Breadcrumbs -->

     <!-- ======= Features Section ======= -->
     <section id="features" class="features" style='margin-top:100px'>
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Fitur dan Layanan</h2>
          <!-- <p>Popular berita</p> -->
        </div>

        <div class="row" data-aos="zoom-in" data-aos-delay="100">
          <div class="col-lg-4 col-md-4">
            <div class="icon-box">
              <i class="ri-police-car-fill" style="color: #ff0000;"></i>
              <h3><a href="/layanan/keamanan">Keamanan</a></h3>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <i class="ri-stethoscope-fill" style="color: #5578ff;"></i>
              <h3><a href="/layanan/kesehatan">Kesehatan</a></h3>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <i class="ri-route-fill" style="color: #e80368;"></i>
              <h3><a href="/layanan/transport">Transport</a></h3>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mt-4">
            <div class="icon-box">
              <i class="ri-basketball-line" style="color: #ff6200;"></i>
              <h3><a href="/layanan/olahraga">Olaraga</a></h3>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mt-4">
            <div class="icon-box">
              <i class="ri-bus-2-line" style="color: #47aeff;"></i>
              <h3><a href="layanan/pemadam-kebakaran">Pemadam Kebakaran</a></h3>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 mt-4">
            <div class="icon-box">
              <i class="ri-earth-fill" style="color: #63ff8d;"></i>
              <h3><a href="layanan/lingkungan-hidup">Lingkungan Hidup</a></h3>
            </div>
          </div>
        </div>

      </div>
    </section>
    <!-- End Features Section -->

  </main>
  <!-- End #main -->

	@endsection
 