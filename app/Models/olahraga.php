<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Olahraga extends Model
{
    protected $table = 'Olahraga';
    protected $fillable = ['title','subtitle','details'];
    // public $timestamps = false;
    use HasFactory;
}
